# 												
# 1. Complete this template table												
"# 2. Save the worksheet as a Text (Tab-delimited) file  -- (use 'File, Save as, Save as type: Text (Tab-delimited)' )"												
# 3. Upload the text file on the 'Attributes' tab of the BioSample Submission Portal at https://submit.ncbi.nlm.nih.gov/subs/biosample/. 												
"# BLUE  fields indicate that at least one of those fields is mandatory. If information is unavailable, please enter 'not collected',  'not applicable' or 'missing' as appropriate."												
"# CAUTION: Be aware that Excel may automatically apply formatting to your data. In particular, take care with dates, incrementing autofills and special characters like / or -. Doublecheck that your text file is accurate before uploading to BioSample."												
"# GREEN fields are mandatory. Your submission will fail if any mandatory fields are not completed. If information is unavailable for any mandatory field, please enter 'not collected',  'not applicable' or 'missing' as appropriate."												
"# Hover over field name to view definition, or see http://www.ncbi.nlm.nih.gov/biosample/docs/attributes/."												
"# If you have any questions, please contact us at biosamplehelp@ncbi.nlm.nih.gov."												
# This is a submission template for batch deposit of 'Microbe; version 1.0' samples to the NCBI BioSample database (http://www.ncbi.nlm.nih.gov/biosample/).												
# TO MAKE A SUBMISSION: 												
# YELLOW fields are optional. Leave optional fields empty (or delete them) if no information is available.												
"# You can add any number of custom fields to fully describe your BioSamples, simply include them in the table."												
*sample_name	sample_title	condition	replicate	deletion	*organism	strain	isolation_source	*collection_date	*geo_loc_name	*sample_type	collected_by	temp
Rm1021WTPHBnegRep1	Rm1021	PHBneg	1	none	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm1021WTPHBnegRep2	Rm1021	PHBneg	2	none	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm1021WTPHBposRep1	Rm1021	PHBpos	1	none	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm1021WTPHBposRep2	Rm1021	PHBpos	2	none	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11134omegaacsA2PHBnegRep1	Rm11134	PHBneg	1	acsA2	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11134omegaacsA2PHBnegRep2	Rm11134	PHBneg	2	acsA2	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11134omegaacsA2PHBposRep1	Rm11134	PHBpos	1	acsA2	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11134omegaacsA2PHBposRep2	Rm11134	PHBpos	2	acsA2	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11347omegaphaBPHBnegRep1	Rm11347	PHBneg	1	phaB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11347omegaphaBPHBnegRep2	Rm11347	PHBneg	2	phaB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11347omegaphaBPHBposRep1	Rm11347	PHBpos	1	phaB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11347omegaphaBPHBposRep2	Rm11347	PHBpos	2	phaB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11401deltabdhAPHBnegRep1	Rm11401	PHBneg	1	bdhA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11401deltabdhAPHBnegRep2	Rm11401	PHBneg	2	bdhA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11401deltabdhAPHBposRep1	Rm11401	PHBpos	1	bdhA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11401deltabdhAPHBposRep2	Rm11401	PHBpos	2	bdhA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11479deltaglgA1PHBnegRep1	Rm11479	PHBneg	1	glgA1	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11479deltaglgA1PHBnegRep2	Rm11479	PHBneg	2	glgA1	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11479deltaglgA1PHBposRep1	Rm11479	PHBpos	1	glgA1	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
Rm11479deltaglgA1PHBposRep2	Rm11479	PHBpos	2	glgA1	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW234deltaphaABPHBnegRep1	SmUW234	PHBneg	1	phaAB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW234deltaphaABPHBnegRep2	SmUW234	PHBneg	2	phaAB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW234deltaphaABPHBposRep1	SmUW234	PHBpos	1	phaAB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW234deltaphaABPHBposRep2	SmUW234	PHBpos	2	phaAB	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW235delphaCPHBnegRep1	SmUW235	PHBneg	1	phaC	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW235delphaCPHBnegRep2	SmUW235	PHBneg	2	phaC	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW235delphaCPHBposRep1	SmUW235	PHBpos	1	phaC	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW235delphaCPHBposRep2	SmUW235	PHBpos	2	phaC	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW236delphaZPHBnegRep1	SmUW236	PHBneg	1	phaZ	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW236delphaZPHBnegRep2	SmUW236	PHBneg	2	phaZ	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW236delphaZPHBposRep1	SmUW236	PHBpos	1	phaZ	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW236delphaZPHBposRep2	SmUW236	PHBpos	2	phaZ	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW41delphaAPHBnegRep1	SmUW41	PHBneg	1	phaA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW41delphaAPHBnegRep2	SmUW41	PHBneg	2	phaA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW41delphaAPHBposRep1	SmUW41	PHBpos	1	phaA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
SmUW41delphaAPHBposRep2	SmUW41	PHBpos	2	phaA	Sinorhizobium meliloti	1021	lab culture	30-Jul-2013	Canada	bacterial culture	Ricardo Nordeste	86 F
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
